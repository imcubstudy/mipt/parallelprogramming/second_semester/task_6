// Task 6:
//     - Write a multi-threaded program that will calculate a number of prime numbers less or equal than given N
//     - N is CLI argument
//     - bonus:
//         - do not use bruteforce
// Comment: bonus task is done!
// Author: Gazizov Yakov

#include <vector>
#include <cstdio>
#include <cstdlib>
#include <cstdint>
#include <cmath>
#include <omp.h>

// get `Sieve of Eratosthenes`.
// Do not use `std::vector<bool>` here as bools are packed and there would be a data race
std::vector<std::uint8_t> getSieve(std::uint64_t const N)
{
    auto sieve = std::vector<std::uint8_t>(N, static_cast<std::uint8_t>(true));
    auto const threshold = static_cast<std::uint64_t>(std::sqrt(N));

    for (std::uint64_t i = 2; i < threshold; ++i) {
        if (sieve[i]) {
            #pragma omp parallel for default(shared)
            for (std::uint64_t j = i * i; j < N; j += i) {
                sieve[j] = static_cast<std::uint8_t>(false);
            }
        }
    }
    return sieve;
}

int main(int const argc, char const *const argv[])
{
    // get the N as argument from CLI
    std::uint64_t N = 0;
    if (argc != 2) {
        std::printf("Argument is not supplied! Or maybe just too mush of them...\n");
        std::exit(EXIT_FAILURE);
    }
    if (auto ret = std::sscanf(argv[1], "%llu", &N); !(ret != EOF && ret == 1) || N < 1) {
        std::printf("Invalid argument was supplied!\n");
        std::exit(EXIT_FAILURE);
    }

    // set dynamic to false just in case if implementation will allocate different number of threads
    // on every other run
    omp_set_dynamic(static_cast<int>(false));

    // output maximum available number of threads
    auto const maxThreads = omp_get_max_threads();
    std::printf("Max threads %d\n", maxThreads);

    // get `Sieve of Eratosthenes`.
    auto const sieve = getSieve(N + 1);

    // calculate amount of prime numbers
    std::uint64_t amount = 0;
    #pragma omp parallel for default(shared) schedule(auto) reduction(+: amount)
    for (std::uint64_t i = 2; i < N + 1; ++i) {
        amount += sieve[i];
    }

    // output result
    std::printf("Amount of primes is %llu\n", amount);

    return 0;
}
